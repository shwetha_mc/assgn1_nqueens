#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"
#define MAX 50 
int count=0,count_seq=0;
int sol=0,sol_seq=0;
static int queen[MAX],num_sol,rank,size,next_proc=1;
MPI_Request req;
MPI_Status stat;
const int msg_count=1000;


int valid(int depth, int *queen, int n, int curr_pos)
{
	int i, v_flag=1;
		for(i=0; i<depth; ++i)
	{
		if((curr_pos == queen[i]) || (abs(curr_pos-queen[i]) == abs(depth-i)))
		{
		v_flag=0;
		}
	}

	return v_flag;	
}


int explore_seq(int depth, int find_until,  int *queen, int n)
{
	 int i,j;

        if(depth<find_until)
        {

         for(i=0;i<n ;i++)
         {
		 printf("line %d\n",__LINE__);
		  printf("line %d sol=%d diff= %d\n ",__LINE__,sol_seq,find_until-depth);
		

	       if(valid(depth,queen,n,i)==1 )
                {
                        printf("%d\n",__LINE__);
			if(sol_seq<=(find_until-depth))
			{
			  queen[depth]=i;
			}
			sol_seq++;
				printf("diff= %d sol=%d\n ",find_until-depth,sol_seq);
                        if(sol_seq==(find_until-depth))
        			{
				printf("line %d\n",__LINE__);
//        			printf("line %d %d\n",__LINE__,(next_proc+1)%size);
			        MPI_Send(queen, n, MPI_INT,1, msg_count,MPI_COMM_WORLD);
				printf("line %d\n",__LINE__);
				sol_seq=0;
				}
			 
			 
	
			 explore_seq(depth+1,find_until,queen,n);
		}
                

                else
                sol_seq=0;

         }
        }
       /* if(sol_seq==(find_until-depth))
        {
	 printf("line %d\n",__LINE__);
//         printf("line %d %d\n",__LINE__,(next_proc+1)%size);
          MPI_Isend(queen, n, MPI_INT,1, msg_count,MPI_COMM_WORLD,&req);
                                                                                            printf("line %d\n",__LINE__);
	} */                                                                                                                             
//
//	printf("line %d \n",__LINE__);
  //       MPI_Isend(queen, n, MPI_INT, 0, msg_count,MPI_COMM_WORLD,&req);
//	 count_seq++;
  //      }

        return count_seq;
}







int explore(int depth, int find_until,  int *queen, int n)
{
	
	int i,j;

	if(depth<=n)
	{

	 for(i=0;i<n ;i++)
	 {
		
		if(valid(depth,queen,n,i)==1 )
		{
			if(sol<=(n-depth))
			
		 	/*	if(sol==(find_until)-depth)
		       		 {
					if(rank==0)
					{
					printf("line %d\n",__LINE__);
					else
					{
					printf("line %d\n",__LINE__);
					MPI_Send(queen, n, MPI_INT, 0, msg_count,MPI_COMM_WORLD);
					}
				}*/
			 	queen[depth]=i;
                        	sol++;
				 printf("line %d\n",__LINE__);
	
			
//		printf("line %d sol %d\n",__LINE__,sol);
			explore(depth+1,find_until,queen,n);
			
		}				
		
		else
		sol=0;
		
	 }	
	}
	if(sol==(n-depth))
	{
	 MPI_Send(queen, n, MPI_INT, 0, msg_count,MPI_COMM_WORLD);
	 printf("line %d\n",__LINE__);

//	fprintf(fil,"rank=%d\n", rank);
//	if(rank==0)
//	{
/*	for(i=0;i<n;++i)
	{
//	MPI_ISend(queen,);
//	printf("%d ",queen[i]);
//	printf("\n");
	//
        fprintf(fil," %d ",queen[i]);
	}*/
//	}
        //fprintf(fil,"\n");
	count++;
	}
	
	return count;
}

int main(int argc, char **argv)
{

	int depth, n,i=0,j,size; 
// 	static int queen[MAX],num_sol,rank;
	FILE *sol;
        MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	//queen[0]=3;
	//sol=fopen("solutions.txt","w");
	
	n=atoi(argv[1]);
	depth=atoi(argv[2]);
	printf("depth input = %d raank=%d\n",depth,rank);
	if(rank==0)
	{
		sol=fopen("solutions.txt","w");
	
		static int sol_recv[MAX];
		
        	
		explore_seq(0,depth,queen,n);
		 printf("line %d\n",__LINE__);

	//	while((i++)<92)
	//	{
	MPI_Recv(sol_recv, n, MPI_INT, 1, msg_count, MPI_COMM_WORLD, &stat);
	//	{	
		 printf("line %d\n",__LINE__);
		
		for(j=0;j<n;++j)
		fprintf(sol,"%d ",sol_recv[j]);
		fprintf(sol,"\n");
	//	}
	//	}		
		 printf("line %d\n",__LINE__);

					
/*		while(i<size)
		{
		MPI_Send(&depth, 1, MPI_INT,i,msg_count, MPI_COMM_WORLD);
 		
	    	MPI_Isend(queen, depth, MPI_INT, i, msg_count,MPI_COMM_WORLD,&req);
		++i;
		}
*/
		
//		MPI_Finalize();

	//	MPI_Wait(&req,&stat);
		fclose(sol);
		 MPI_Finalize();

	}
	else
	{
		 printf("line %d\n",__LINE__);

//		MPI_Recv(&depth, 1, MPI_INT, 0,msg_count, MPI_COMM_WORLD, &stat);
	//	msg_count++;
		MPI_Recv(queen, n, MPI_INT, 0, msg_count, MPI_COMM_WORLD, &stat);
		printf("line %d\n",__LINE__);

	//	msg_count++;
		num_sol=explore(depth,n,queen,n);
	}
	printf("Number of solutions = %d\n",num_sol);
	MPI_Finalize();
//	 fclose(sol);

}
