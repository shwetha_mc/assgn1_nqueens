MPICC=mpicc

queens: queens.c
	$(MPICC) -o $@ $<

.PHONY: clean

clean:
	rm -f queens *.o *~

