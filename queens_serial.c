#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"
#define MAX 10 
int count=0;
int sol=0;
int valid(int depth, int *queen, int n, int curr_pos)
{
	int i, v_flag=1;
		for(i=0; i<depth; ++i)
	{
		if((curr_pos == queen[i]) || (abs(curr_pos-queen[i]) == abs(depth-i)))
		{
		v_flag=0;
		}
	}

	return v_flag;	
}

int explore(int depth, int *queen, int n, FILE *fil)
{
	int i,j;
	if(depth<=n )
	{

	 for(i=0;i<n ;i++)
	 {
		
		if(valid(depth,queen,n,i)==1 )
		{
			if(sol<=(n-depth))
			queen[depth]=i;
			sol++;
			explore(depth+1,queen,n,fil);
		}				
		
		else
		sol=0;
		
	 }	
	}
	if(sol==(n-depth))
	{
	for(i=0;i<n;++i)
        fprintf(fil,"%d ",queen[i]);
        fprintf(fil,"\n");
	count++;
	}
	return count;
}

int main(int argc, char **argv)
{

	int depth=0, n,msg_count,i,num_sol; 
	int queen[MAX];
//	queen[0]=3;
//	queen[1]=0;
	n=atoi(argv[1]);
	FILE *sol;
	sol=fopen("serial_olutions.txt","w");
//	MPI_Status stat;
//	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
//	MPI_Recv(&depth, 1, MPI_INT,0, rank+msg_count, MPI_COMM_WORLD, &stat);
	msg_count++;
	
//	MPI_Recv(queen, depth, MPI_INT, 0, rank+msg_count, MPI_COMM_WORLD, &stat);
	msg_count++;
	num_sol=explore(depth,queen,n,sol);
	printf("Number of solutions = %d\n",num_sol);
}
