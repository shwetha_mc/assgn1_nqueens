#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"
#define MAX 10 
int count=0;
int sol=0;
int valid(int depth, int *queen, int n, int curr_pos)
{
	int i, v_flag=1;
		for(i=0; i<depth; ++i)
	{
		if((curr_pos == queen[i]) || (abs(curr_pos-queen[i]) == abs(depth-i)))
		{
		v_flag=0;
		}
	}

	return v_flag;	
}

int explore(int depth, int *queen, int n, FILE *fil)
{
	int i,j;
	if(depth<=n )
	{

	 for(i=0;i<n ;i++)
	 {
		
		if(valid(depth,queen,n,i)==1 )
		{
			if(sol<=(n-depth))
			queen[depth]=i;
			sol++;
	//		printf("line %d sol %d\n",__LINE__,sol);
			explore(depth+1,queen,n,fil);
		}				
		
		else
		sol=0;
		
	 }	
	}
	if(sol==(n-depth))
	{
	for(i=0;i<n;++i)
	printf("%d ",queen[i]);
	printf("\n");
	//
        //fprintf(fil,"%d ",queen[i]);
        //fprintf(fil,"\n");
	count++;
	}
	return count;
}

int main(int argc, char **argv)
{

	const int msg_count=1000;
	int depth=0, n,i,size; 
 	static int queen[MAX],num_sol,rank;
	FILE *sol;
        MPI_Init(&argc,&argv);
	MPI_Request req;

	MPI_Status stat;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	printf("line num %d\n",__LINE__);
	queen[0]=3;
//	queen[1]=0;
	
	n=atoi(argv[1]);
	if(rank==0)
	{
		 sol=fopen("solutions.txt","w");

		while(i<size)
		{
		MPI_Send(&depth, 1, MPI_INT,i,msg_count, MPI_COMM_WORLD);
 	    	MPI_Isend(queen, depth, MPI_INT, i, msg_count,MPI_COMM_WORLD,&req);
		++i;
		}
		printf("Send complete \n");
//		MPI_Finalize();

	//	MPI_Wait(&req,&stat);
	}
	else
	{
		printf("%d\n",rank);
		MPI_Recv(&depth, 1, MPI_INT, 0,msg_count, MPI_COMM_WORLD, &stat);
		printf("Post first receive\n");
	//	msg_count++;
		MPI_Recv(queen, depth, MPI_INT, 0, msg_count, MPI_COMM_WORLD, &stat);
	printf("%d\n",queen[0]);
	//	msg_count++;
		num_sol=explore(depth,queen,n,sol);
	}
	printf("Number of solutions = %d\n",num_sol);
	MPI_Finalize();
	 fclose(sol);

}
